"""
This script implements a preprocessing pipeline to have the data ready
to use for classification
"""

import math
import numpy as np


PILOTS = ['p02', 'p03', 'p04', 'p05', 'p06', 'p07', 'p08', 'p09', 'p11']
TASKS = ['task1', 'task2', 'task3']


def get_extremums(file_reader):
    """
    Get minimums and maximums
    """
    # Go to the beginning of the file
    file_reader.seek(0)

    # Prepare lists
    max_x0 = [-math.inf for i in range(16)]
    min_x0 = [math.inf for i in range(16)]

    # Search for max and min in the whole file
    file_reader.readline()  # handle headers
    for line in file_reader:
        x0 = line.split(',')[1:]
        x0 = [float(i) for i in x0]
        max_x0 = np.maximum(max_x0, x0)
        min_x0 = np.minimum(min_x0, x0)

    # # Print in console
    # print('max: ' + str(max_x0))
    # print('min: ' + str(min_x0))

    # Return min and max
    return min_x0, max_x0


def normalize(file_reader, file_writer,  min_x0, max_x0):
    """
    Normalize between 0 and 1
    """
    # Go to the beginning of the file
    file_reader.seek(0)

    # Get range
    range_x0 = max_x0 - min_x0

    # Handle headers
    headers = file_reader.readline()
    file_writer.write(headers)

    for line in file_reader:
        # Normalize the data
        sample = line.split(',')[0]
        x0 = line.split(',')[1:]
        x0 = np.array([float(i) for i in x0])
        normalized_x0 = (x0 - min_x0) / range_x0

        # Write in the normalized file
        file_writer.write(sample)
        for channel in normalized_x0:
            file_writer.write(',' + str(round(channel, 3)))
        file_writer.write('\n')


def av_sd(file_reader, file_writer):
    """
    Get mean and standard deviation of 4 nearby channels
    """
    # Go to the beginning of the file
    file_reader.seek(0)

    # Handle headers
    file_reader.readline()
    headers = 'Sample,Av1,Sd1,Av2,Sd2,Av3,Sd3,Av4,Sd4'
    file_writer.write(headers + '\n')

    for line in file_reader:
        sample = line.split(',')[0]
        tx1 = line.split(',')[1:5]
        tx1 = [float(i) for i in tx1]
        tx1a = str(np.nanmean(tx1))
        tx1s = str(np.nanstd(tx1))
        tx2 = line.split(',')[5:9]
        tx2 = [float(i) for i in tx2]
        tx2a = str(np.nanmean(tx2))
        tx2s = str(np.nanstd(tx2))
        tx3 = line.split(',')[9:13]
        tx3 = [float(i) for i in tx3]
        tx3a = str(np.nanmean(tx3))
        tx3s = str(np.nanstd(tx3))
        tx4 = line.split(',')[13:17]
        tx4 = [float(i) for i in tx4]
        tx4a = str(np.nanmean(tx4))
        tx4s = str(np.nanstd(tx4))

        # Write in the new file
        towrite = (sample + ',' + tx1a + ',' + tx1s + ',' + tx2a + ',' +
                   tx2s + ',' + tx3a + ',' + tx3s + ',' + tx4a + ',' +
                   tx4s + '\n')
        file_writer.write(towrite)


def main():
    print("Processing:")
    for pilot in PILOTS:
        print('Pilot: {}'.format(pilot))
        for task in TASKS:
            print('\tTask: {}'.format(task))
            # Open files for normalization
            cbsi_reader = open('../data/'+pilot+'/'+task+'.csv', 'r')
            norm_writer = open('../data/'+pilot+'/'+task+'_norm.csv', 'w')
            # Normalize
            min_x0, max_x0 = get_extremums(cbsi_reader)
            normalize(cbsi_reader, norm_writer, min_x0, max_x0)
            # Close files
            cbsi_reader.close()
            norm_writer.close()

            # Open files for average and standard deviation
            norm_reader = open('../data/'+pilot+'/'+task+'_norm.csv', 'r')
            navsd_writer = open('../data/'+pilot+'/'+task+'_navsd.csv', 'w')
            # Average and compute standard deviation
            av_sd(norm_reader, navsd_writer)
            # Close files
            norm_reader.close()
            navsd_writer.close()


if __name__ == '__main__':
    main()
